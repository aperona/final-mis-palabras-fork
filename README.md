# Final MisPalabras

# Entrega practica
## Datos
* Nombre: Antonio Perona Martínez (aperona)
* Titulación: Ingeniería Telemática
* Despliegue (url): http://aperona.pythonanywhere.com/MisPalabras/
* Video básico (url): https://www.youtube.com/watch?v=HXCtkPBJEQg
* Video parte opcional (url): https://www.youtube.com/watch?v=ZylP1wZzlrY
## Cuenta Admin Site
* admin/admin
## Cuentas usuarios
* UserTest/UserTest
* UserTest2/UserTest2

## Resumen parte obligatoria

En esta página web MisPalabras, los usuarios, autenticados o no, podrán ver en el inicio una lista de todas las palabras almacenadas en el sitio paginadas de 5 en 5 y ordenadas de la más nueva a la más antigua. Asimismo, podrán buscar cualquier palabra y acceder a su página, logrando obtener su definición e imagen de Wikipedia en Español, si la tuviera. También podrán acceder a la información de todas las palabras alamacenadas en formato JSON y XML, además de tener una página de ayuda.

También, para todas las páginas, el usuario podrá logearse o cerrar su sesión, seleccionar una opción posible dentro de la barra de opciones, buscar una palabra, ver la lista de las 10 palabras del sitio más votadas o ver el pie de página con el número de palabras almacenadas y un ejemplo aleatorio de estas.

Sólo las palabras añadidas en la página podrán ser votadas y comentadas por los usuarios autenticados.

Sólo los usuarios autenticados podrán añadir nuevas palabras, votarlas y comentarlas, también podrán compartir enlaces. Además de tener acceso a su página de usuario donde se mostrará toda actividad realizada por este, ya sean palabras añadidas, comentarios puestos, enlaces compartidos...


## Lista partes opcionales

* Publicación de imágenes en páginas de palabras: Los usuarios autenticados podrán compartir imágenes en las páginas de las palabras rellenando un formulario en el que introduzcan el enlace de la imagen, ante esto, se mostrará la pertinente imagen en su sección correspondiente. Asimismo, cada usuario podrá ver las imágenes que ha compartido en cada palabra en su página de usuario en orden de la más nueva a la más antigua. 


## Anotación Commits

Dado un problema que tuve con el GitLab, los commits realizados hasta los últimos días de la entrega los realicé en un repositorio creado por mí en GitHub:

https://github.com/aperona2018/temp-ST-web

