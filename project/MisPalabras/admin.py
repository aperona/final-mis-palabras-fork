from django.contrib import admin
from .models import Palabra, Comentario, Voto, Enlace, Imagen

# Register your models here.
admin.site.register(Palabra)
admin.site.register(Comentario)
admin.site.register(Voto)
admin.site.register(Enlace)
admin.site.register(Imagen)

